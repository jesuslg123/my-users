# My Users

[![N|My Users](https://strategicsalesmarketingosmg.files.wordpress.com/2012/06/shutterstock_59234440.jpg)](htts://bitbucket.org/jesuslg123/my-users/)

*My users* is a simple iOS Swift client for [Random.me API](randomuser.me)

  - Infine users scroll list .
  - User details view, tapping on image user.
  - Allow to delete user swiping.
  - Filter list by name, lastname or email.


### Before compile

*My Users* use [Cocopods](https://cocoapods.org) for AlamofireImage library integration, before open the projet, pod install is required:

```sh
$ cd TO_PROJECT_FOLDER
$ pod install
```

Now you can use the project, opening always the file:
```
MyRandomUsers.xcworkspace
```
