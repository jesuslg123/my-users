//
//  UsersListDataSourceTests.swift
//  MyRandomUsersTests
//
//  Created by Jesus Lopez Garcia on 02/12/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import XCTest


class UsersListDataSourceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //MARK: Add user
    
    func testGivenNoDuplicateUsers_ThenGetSameLengthData() {
        let users:[User] = fakeUsers(total:10)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)
        
        let totalElements = dataSource.totalElements()
        
        XCTAssertEqual(10, totalElements, "User has not been added correctly")
    }
    
    //MARK: Add users with duplicates
    
    func testGivenFewDuplicatesUsers_ThenGetNonDuplicateLengthData() {
        let users:[User] = fakeUsers(total:3)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)
        
        let moreUsersWithDuplicates:[User] = fakeUsers(total:6)
        dataSource.addData(users: moreUsersWithDuplicates)
        
        let totalElements = dataSource.totalElements()
        
        XCTAssertEqual(6, totalElements, "Users has not been filtered by duplicate correctly")
    }
    
    //MARK: Filter
    
    func testGivenFewDuplicatesUsers_WhenFilterByName_ThenGetNonDuplicateAndFilteredLengthData() {
        let users:[User] = fakeUsers(total:3)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)
        
        let moreUsersWithDuplicates:[User] = fakeUsers(total:6)
        dataSource.addData(users: moreUsersWithDuplicates)
        
        dataSource.setFilter(type: .name, value: "User_2")
        let totalElements = dataSource.totalElements()
        
        XCTAssertEqual(1, totalElements, "Users has not been filtered by name and duplicate correctly")
    }
    
    func testGivenUsers_WhenFilterByName_ThenGetFilteredLengthData() {
        let users:[User] = fakeUsers(total:30)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)
        
        dataSource.setFilter(type: .name, value: "User_2")
        let totalElements = dataSource.totalElements()
        
        XCTAssertEqual(11, totalElements, "Users has not been filtered by name correctly")
    }
    
    func testGivenUsersWithDuplicates_WhenFilterByName_ThenGetFilteredLengthData() {
        let users:[User] = fakeUsers(total:30)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)
        
        dataSource.setFilter(type: .name, value: "User_2")
        let totalElements = dataSource.totalElements()
        
        let moreUsersWithDuplicates:[User] = fakeUsers(total:6)
        dataSource.addData(users: moreUsersWithDuplicates)
        
        XCTAssertEqual(11, totalElements, "Users has not been filtered by name and duplicate correctly")
    }
    
    func testGivenFewDuplicatesUsers_WhenFilterByLastname_ThenGetNonDuplicateAndFilteredLengthData() {
        let users:[User] = fakeUsers(total:3)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)
        
        let moreUsersWithDuplicates:[User] = fakeUsers(total:6)
        dataSource.addData(users: moreUsersWithDuplicates)
        
        dataSource.setFilter(type: .lastname, value: "lastname_2")
        let totalElements = dataSource.totalElements()
        
        XCTAssertEqual(1, totalElements, "Users has not been filtered by lastname and duplicate correctly")
    }
    
    func testGivenFewDuplicatesUsers_WhenFilterByEmail_ThenGetNonDuplicateAndFilteredLengthData() {
        let users:[User] = fakeUsers(total:3)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)
        
        let moreUsersWithDuplicates:[User] = fakeUsers(total:6)
        dataSource.addData(users: moreUsersWithDuplicates)
        
        dataSource.setFilter(type: .email, value: "email_2")
        let totalElements = dataSource.totalElements()
        
        XCTAssertEqual(1, totalElements, "Users has not been filtered by email correctly")
    }

    //MARK: Disable user
    
    func testGivenUsers_WhenDisableAUser_ThenGetNonDeleteUserLength() {
        let users:[User] = fakeUsers(total:3)
        
        let dataSource:UsersListDataSource = UsersListDataSource()
        dataSource.addData(users: users)

        dataSource.markUserAsDisabled(atIndexPath: IndexPath(row: 0, section: 0))
        
        let totalElements = dataSource.totalElements()
        
        XCTAssertEqual(2, totalElements, "User has not been disabled correctly")
    }
    
    //MARK: Mock data
    
    func fakeUsers(total:Int) -> [User] {
        var users:[User] = [User]()
        
        var index:Int = 0
        while index < total {
            users.append(fakeUser(index:index))
            index += 1
        }
        return users
    }
    
    func fakeUser(index:Int) -> User {
        let user:User = User()
        
        user.name = "User_\(index)"
        user.lastname = "lastname_\(index)"
        user.email = "email_\(index)"
        
        return user
    }
    
}
