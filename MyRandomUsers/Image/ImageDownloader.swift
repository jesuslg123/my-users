//
//  ImageDownloader.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 02/12/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

class ImageDownloader: NSObject {

    private var dataTask:URLSessionDataTask?
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        dataTask?.cancel()
        dataTask = URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }
        dataTask?.resume()
    }
    
}
