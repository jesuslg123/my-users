//
//  UsersInteractor.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

class UsersInteractor: NSObject {

    private var currentPage:Int = 0
    private var isLoading:Bool = false
    private var hasPendingItems:Bool = true
    
    //MARK: Public methods
    
    func fetchUsersNextPage(completionHandler:@escaping(Error?,  [User]) -> ()) {
        if (!canRequestMore()) {
            return
        }
        
        fetchUsers(page: currentPage) { (error, result) in
            self.isLoading = false
            if (error != nil && result.count == 0) {
                self.hasPendingItems = false
            }
            
            if (error == nil && result.count > 0) {
                self.currentPage += 1
            }
            
            completionHandler(error, result)
        }
    }
    
    func fetchUsers(page:Int=0, size:Int=30, completionHandler:@escaping(Error?,  [User]) -> ()) {
        fetchUsersNetwork(page: page, size: size, completionHandler: completionHandler)
    }
    
    //MARK: Network
    
    private func fetchUsersNetwork(page:Int=0, size:Int=30, completionHandler:@escaping(Error?, [User]) -> ()) {
        var params:Dictionary = [String:String]()
        params["page"] = String(page)
        params["results"] = String(size)
        
        NetworkManager(aHost:"https://api.randomuser.me").get(endPoint: "", parameters: params) { (error, result) in
            var users = [User]()
            if let data = result {
                users = self.parseResult(data: data)
            }
            completionHandler(error, users)
        }
    }
    
    func canRequestMore() -> Bool {
        return (!isLoading && hasPendingItems)
    }
    
    //MARK: Parse
    
    private func parseResult(data:AnyObject) -> [User] {
        var users = [User]()
        let results = data["results"] as! [AnyObject]
        for object in results {
            let user = User(data: object as! Dictionary)
            users.append(user)
        }
        return users
    }
    
}
