//
//  UserDetailsViewController.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController {

    @IBOutlet var imageViewUserPicture: UIImageView!
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelLocation: UILabel!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelEmail: UILabel!
    @IBOutlet var labelGender: UILabel!
    @IBOutlet var imageViewGender: UIImageView!
    
    
    var currentUser:User = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI(user: currentUser)
    }

    //MARK: Configure UI
    
    func configureUI(user:User) {
        initializeName(user: user)
        initializeEmail(user: user)
        initializeGender(user: user)
        initializeDate(user: user)
        initializeLocation(location: user.location)
        imageViewUserPicture.initializePictureUrl(urlString: user.pictureUrl, placeholder:#imageLiteral(resourceName: "Users"))
    }
    
    func initializeName(user:User) {
        labelName.text  = "\(user.name.capitalized) \(user.lastname.capitalized)"
    }
    
    func initializeEmail(user:User) {
        labelEmail.text  = "\(user.email)"
    }
    
    func initializeLocation(location:Location) {
        labelLocation.text  = "\(location.street)\n\(location.city.capitalized), \(location.state.capitalized)"
    }
    
    func initializeDate(user:User) {
        labelDate.text  = "\(user.date.toString())"
    }
    
    func initializeGender(user:User) {
        var image = #imageLiteral(resourceName: "Users")
        
        switch user.gender {
        case .female:
            image = #imageLiteral(resourceName: "Female")
        case .male:
            image = #imageLiteral(resourceName: "Male")
        default:
            image = #imageLiteral(resourceName: "Users")
        }
        
        imageViewGender.image = image
    }
    

}
