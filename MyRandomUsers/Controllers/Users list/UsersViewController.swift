//
//  UsersViewController.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController, UITableViewDelegate, UsersListDataSourceDelegate {

    
    @IBOutlet var textFieldSearch: UITextField!
    @IBOutlet var buttonFilters: UIButton!
    @IBOutlet var tableViewUsers: UITableView!
    
    let usersInteractor = UsersInteractor()
    let dataSource = UsersListDataSource()
    
    var timerText:Timer?
    var currentFilter:Filter = .disabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initialize()
    }

    //MARK: Setup
    
    func initialize() {
        initializeTableView()
        disableTextField()
        requestData()
        
    }
    
    func requestData() {
        usersInteractor.fetchUsersNextPage { (error, users) in
            if let err = error {
                self.showError(title: "Ops!", message: err.localizedDescription)
            } else {
                self.updateTableData(data: users)
            }
        }
        
    }
    
    //MARK: Actions
    
    @IBAction func onButtonFilterAction(_ sender: UIButton) {
        showFilterAlerts(sender: sender)
    }
    
    @IBAction func onTextFieldValueChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        filterByText(string: text)
    }
    
    //MARK: TableView
    
    func initializeTableView() {
        dataSource.delegate = self
        tableViewUsers.delegate = self
        tableViewUsers.dataSource = dataSource
        tableViewUsers.register(UINib(nibName: UserTableViewCell.nibName(), bundle: nil), forCellReuseIdentifier: UserTableViewCell.cellIndentifier())
        tableViewUsers.allowsMultipleSelectionDuringEditing = false
    }
    
    func updateTableData(data:Array<User>) {
        dataSource.addData(users: data)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row > self.dataSource.totalElements() - 3) {
            requestData()
        }
    }
    
    
    //MARK: UsersListDataSourceDelegate
    
    func usersListDataSourceUserSelected(dataSource: UsersListDataSource, user: User) {
        showUserDetails(user: user)
    }
    
    func usersListDataSourceChanged(dataSource: UsersListDataSource) {
        DispatchQueue.main.async {
            self.reloadAll()
        }
    }
    
    func reloadAll() {
        tableViewUsers.reloadData()
    }

    func showUserDetails(user:User) {
        let userDetailsViewController:UserDetailsViewController = UserDetailsViewController()
        userDetailsViewController.currentUser = user
                
        show(userDetailsViewController, sender: nil)
    }
    
    //MARK: Filters
    
    func filterByText(string:String) {
        timerText?.invalidate()
        timerText = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false, block:{ (timer) in
            self.filter(type: self.currentFilter, text: string)
            timer.invalidate()
        })
    }
    
    func showFilterAlerts(sender:UIButton) {
        let filterSelectorAlert:UIAlertController = UIAlertController(title: "Filter By", message: nil, preferredStyle: .actionSheet)
        
        filterSelectorAlert.addAction(UIAlertAction(title: Filter.name.rawValue, style: .default, handler: { (alertView) in
            self.setFilterSelected(aType: .name)
        }))
        
        filterSelectorAlert.addAction(UIAlertAction(title: Filter.lastname.rawValue, style: .default, handler: { (alertView) in
            self.setFilterSelected(aType: .lastname)
        }))
        
        filterSelectorAlert.addAction(UIAlertAction(title: Filter.email.rawValue, style: .default, handler: { (alertView) in
            self.setFilterSelected(aType: .email)
        }))
        
        filterSelectorAlert.addAction(UIAlertAction(title: Filter.disabled.rawValue, style: .destructive, handler: { (alertView) in
            self.setFilterSelected(aType: .disabled)
        }))
        
        filterSelectorAlert.popoverPresentationController?.sourceView = sender
        filterSelectorAlert.popoverPresentationController?.sourceRect = sender.bounds
        
        present(filterSelectorAlert, animated: true, completion: nil)
    }

    func setFilterSelected(aType:Filter) {
        if (aType == .disabled) {
            disableTextField()
            filter(type: .disabled, text: "")
            buttonFilters.setTitle("Filter", for: .normal)
        } else {
            enableTextField()
            buttonFilters.setTitle(aType.rawValue.capitalized, for: .normal)
        }
        currentFilter = aType
    }
    
    func filter(type:Filter, text:String) {
        dataSource.setFilter(type: type, value: text)
    }
    
    func disableTextField() {
        textFieldSearch.isEnabled = false
        textFieldSearch.text = ""
        textFieldSearch.alpha = 0.5
    }
    
    func enableTextField() {
        textFieldSearch.isEnabled = true
        textFieldSearch.alpha = 1
    }

}

