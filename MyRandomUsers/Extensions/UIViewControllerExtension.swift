//
//  UIViewControllerExtension.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showError(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            
        }))
        present(alert, animated: true, completion: nil)
    }
    
}
