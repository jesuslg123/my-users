//
//  UIImageViewExtension.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 27/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImageView {
    
    func initializePictureUrl(urlString:String, placeholder:UIImage) {
        image = placeholder
        loadUrlPicture(urlText:urlString)
    }
    
    private func loadUrlPicture(urlText:String?) {
        guard let stringUrl = urlText else { return }
        guard let url = URL(string:stringUrl) else { return }
        
        self.af_setImage(
            withURL: url,
            placeholderImage: #imageLiteral(resourceName: "Users"),
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }

}
