//
//  DateExtension.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

extension Date {
    
    func toString(timeStyle:DateFormatter.Style = .medium, dateStyle:DateFormatter.Style = .medium ) -> String {
        let dateFormater:DateFormatter = DateFormatter()
        dateFormater.timeStyle = timeStyle
        dateFormater.dateStyle = dateStyle
        return dateFormater.string(from: self)
    }
    
}
