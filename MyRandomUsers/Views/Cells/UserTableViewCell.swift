//
//  UserTableViewCell.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

protocol UserTableViewCellDelegate {
    func imageUserTableViewCellTapped(sender:UserTableViewCell, user:User)
}

class UserTableViewCell: UITableViewCell {

    @IBOutlet var imageViewPicture: UIImageView!
    @IBOutlet var labelFullName: UILabel!
    @IBOutlet var imageViewGender: UIImageView!
    @IBOutlet var labelPhone: UILabel!
    @IBOutlet var labelLocation: UILabel!
    
    var delegate:UserTableViewCellDelegate?
    
    private var user:User = User()
    
    
    //MARK: Class methods
    
    class func cellIndentifier() -> String {
        return "UserCell"
    }
    
    class func nibName() -> String {
        return String(describing: self)
    }
    
    //MARK: Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initImageTapGesture()
    }
    
    //MARK: Configuration
    
    func configure(withUser:User) {
        user = withUser
        initializeName(user: withUser)
        initializePhone(user: withUser)
        initializeLocation(location: withUser.location)
        initializeGender(user: withUser)
        imageViewPicture.initializePictureUrl(urlString: user.thumbnailUrl, placeholder:#imageLiteral(resourceName: "Users"))
    }
    
    func initializeName(user:User) {
        labelFullName.text  = "\(user.lastname.capitalized), \(user.name.capitalized)"
    }
    
    func initializePhone(user:User) {
        labelPhone.text  = "\(user.phone)"
    }
    
    func initializeLocation(location:Location) {
        labelLocation.text  = "\(location.city.capitalized), \(location.state.capitalized)"
    }
    
    func initializeGender(user:User) {
        var image = #imageLiteral(resourceName: "Users")
        
        switch user.gender {
        case .female:
            image = #imageLiteral(resourceName: "Female")
        case .male:
            image = #imageLiteral(resourceName: "Male")
        default:
            image = #imageLiteral(resourceName: "Users")
        }
        
        imageViewGender.image = image
    }

    
    func initImageTapGesture() {
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(onPictureTapGesture(_:)))
        tap.numberOfTapsRequired = 1
        
        imageViewPicture.isUserInteractionEnabled = true
        imageViewPicture.addGestureRecognizer(tap)
    }
    
    //MARK: Actions
    
    @objc func onPictureTapGesture(_ sender: UITapGestureRecognizer) {
        notifyImageTapped(user: user)
    }
    
    
    //MARK: UserTableViewCellDelegate
    
    func notifyImageTapped(user:User) {
        delegate?.imageUserTableViewCellTapped(sender: self, user: user)
    }


}
