//
//  Location.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

class Location: NSObject {

    var street:String = ""
    var city:String = ""
    var state:String = ""
    
    override init() {
        super.init()
    }
    
    init(data:Dictionary<String, Any>) {
        super.init()
        
        parseDictionary(dictionary: data)
    }
    
    //MARK: Parse
    
    private func parseDictionary(dictionary: [String : Any]) {
        if let jsonStreet = dictionary["street"] as? String {
            street = jsonStreet
        }
        
        if let jsonCity = dictionary["city"] as? String {
            city = jsonCity
        }
        
        if let jsonState = dictionary["state"] as? String {
            state = jsonState
        }
    }
    
}
