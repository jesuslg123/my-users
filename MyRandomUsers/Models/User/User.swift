//
//  User.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

class User: NSObject {
    
    enum Gender:String {
        case unknown = "unknown"
        case male = "male"
        case female = "female"
    }
    
    var name:String = ""
    var lastname:String = ""
    var email:String = ""
    var phone:String = ""
    var pictureUrl:String = ""
    var thumbnailUrl:String = ""
    var gender:Gender = .unknown
    var date:Date = Date()
    var location:Location = Location()
    var enabled:Bool = true
    
    var id:String {
        get {
            return "\(name)\(lastname)"
        }
    }
    

    override init() {
        super.init()
    }
    
    init(data:Dictionary<String, Any>) {
        super.init()
        
        parseDictionary(dictionary: data)
    }
    
    //MARK: Parse
    
    private func parseDictionary(dictionary:Dictionary<String, Any>) {
        parseGender(dictionary)
        
        parseEmail(dictionary)
        
        parsePhone(dictionary)
        
        parseDate(dictionary)
        
        parseName(dictionary)
        
        parseLocation(dictionary)
        
        parsePicture(dictionary)
    }
    
    private func parseGender(_ dictionary: [String : Any]) {
        if let jsonGender:String = dictionary["gender"] as? String {
            gender = User.Gender(rawValue: jsonGender)!
        }
    }
    
    private func parseEmail(_ dictionary: [String : Any]) {
        if let jsonEmail:String = dictionary["email"] as? String {
            email = jsonEmail
        }
    }
    
    private func parsePhone(_ dictionary: [String : Any]) {
        if let jsonPhone:String = dictionary["phone"] as? String {
            phone = jsonPhone
        }
    }
    
    private func parseDate(_ dictionary: [String : Any]) {
        if let jsonRegistered:String = dictionary["registered"] as? String {
            let dateFormatter:DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss" //2013-10-07 20:19:48
            if let dateJson = dateFormatter.date(from: jsonRegistered) {
                date = dateJson
            }
        }
    }
    
    private func parseName(_ dictionary: [String : Any]) {
        if let jsonNameDictionary:Dictionary<String, Any> = dictionary["name"] as? Dictionary {
            if let jsonName = jsonNameDictionary["first"] as? String {
                name = jsonName
            }
            
            if let jsonLastName = jsonNameDictionary["last"] as? String {
                lastname = jsonLastName
            }
        }
    }
    
    private func parseLocation(_ dictionary: [String : Any]) {
        if let jsonLocationDictionary:Dictionary<String, Any> = dictionary["location"] as? Dictionary {
            location = Location(data: jsonLocationDictionary)
        }
    }
    
    private func parsePicture(_ dictionary: [String : Any]) {
        if let jsonPictureDictionary:Dictionary<String, Any> = dictionary["picture"] as? Dictionary {
            if let jsonThumbnail = jsonPictureDictionary["thumbnail"] as? String {
                thumbnailUrl = jsonThumbnail
            }
            
            if let jsonLarge = jsonPictureDictionary["large"] as? String {
                pictureUrl = jsonLarge
            }
        }
    }
    
}
