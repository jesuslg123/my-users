//
//  UsersListDataSource.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit

enum Filter:String {
    case disabled = "disabled"
    case name = "name"
    case lastname = "lastname"
    case email = "email"
}

protocol UsersListDataSourceDelegate {
    func usersListDataSourceUserSelected(dataSource:UsersListDataSource, user: User)
    func usersListDataSourceChanged(dataSource:UsersListDataSource)
}

class UsersListDataSource: NSObject, UITableViewDataSource, UserTableViewCellDelegate {
    
    
    private var originalData:[User] = []
    private var currentData:[User] = []
    private var filter:Filter = .disabled
    private var filterString:String = ""
    
    public var delegate:UsersListDataSourceDelegate?
    
    
    //MARK: Public methods
    
    func addData(users:[User]) {
        addNonDuplicateData(users:originalData, newUsers: users)
        
        updateCurrentData()
    }
    
    func setFilter(type:Filter, value:String) {
        filter = type
        filterString = value
        
        updateCurrentData()
    }
    
    func updateCurrentData() {
        currentData = processArray(users: originalData)
        notifyDataChanged()
    }
    
    func totalElements() -> Int {
        return currentData.count
    }
    
    
    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user:User = currentData[indexPath.row]
        
        let cell:UserTableViewCell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.cellIndentifier(), for: indexPath) as! UserTableViewCell
        cell.delegate = self
        cell.configure(withUser: user)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            markUserAsDisabled(atIndexPath: indexPath)
        default:
            return
        }
    }
    
    
    //MARK: UserTableViewCellDelegate
    
    func imageUserTableViewCellTapped(sender: UserTableViewCell, user: User) {
        notifyCellSelected(user: user)
    }
    
    
    //MARK: UsersListDataSourceDelegate
    
    func notifyCellSelected(user: User) {
        delegate?.usersListDataSourceUserSelected(dataSource: self, user: user)
    }
    
    func notifyDataChanged() {
        delegate?.usersListDataSourceChanged(dataSource: self)
    }

    //MARK: Filter
    
    private func processArray(users:[User]) -> [User] {
        var data = users
        data = filter(users: data, type:filter , value: filterString)
        data = sortUsersByName(users: data)
        return data
    }
    
    private func filterDisabledUsers(users:[User]) -> [User] {
        return users.filter { (user) -> Bool in
            return user.enabled
        }
    }
    
    private func filter(users:[User], type:Filter, value:String) -> [User] {
        if (value == "") {
            return filterDisabled()
        }
        
        switch type {
        case .name:
            return filterByName(users: users, value: value)
        case .lastname:
            return filterByLastname(users: users, value: value)
        case .email:
            return filterByEmail(users: users, value: value)
        case .disabled:
            return filterDisabled()
        }
    }
    
    private func filterDisabled() -> [User] {
        return sortUsersByName(users: filterDisabledUsers(users: originalData))
    }
    
    private func filterByName(users:[User], value:String) -> [User] {
        return users.filter { (user) -> Bool in
            return (user.enabled && user.name.localizedLowercase.range(of: value.localizedLowercase) != nil)
        }
    }
    
    private func filterByLastname(users:[User], value:String) -> [User] {
        return users.filter { (user) -> Bool in
            return (user.enabled && user.lastname.localizedLowercase.range(of: value.localizedLowercase) != nil)
        }
    }
    
    private func filterByEmail(users:[User], value:String) -> [User] {
        return users.filter { (user) -> Bool in
            return (user.enabled && user.email.localizedLowercase.range(of: value.localizedLowercase) != nil)
        }
    }
    
    
    //MARK: Sort
    
    private func sortUsersByName(users:[User]) -> [User] {
        return users.sorted(by: { (userA, userB) -> Bool in
            return userA.name.localizedCompare(userB.name) == .orderedAscending
        })
    }
    
    
    //MARK: Delete
    
    func markUserAsDisabled(atIndexPath:IndexPath) {
        let userToDisable = currentData[atIndexPath.row]
        let user = originalData.first { (user) -> Bool in
            return user.id == userToDisable.id
        }
        if let user = user {
            user.enabled = false
            originalData[atIndexPath.row] = user
        }
        
        updateCurrentData()
    }
    
    //MARK: Add
    
    private func addNonDuplicateData(users:[User], newUsers:[User]) {
        for newUser in newUsers {
            if (!isUserDuplicate(users:users, existUser: newUser)) {
                originalData.append(newUser)
            }
        }
    }
    
    //MARK: Duplicate
    
    private func isUserDuplicate(users:[User], existUser:User) -> Bool {
        return users.contains { (user) -> Bool in
            return existUser.id == user.id
        }
    }

}
