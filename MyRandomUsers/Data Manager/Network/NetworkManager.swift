//
//  NetworkManager.swift
//  MyRandomUsers
//
//  Created by Jesus Lopez Garcia on 26/11/2017.
//  Copyright © 2017 Jesus Lopez Garcia. All rights reserved.
//

import UIKit


class NetworkManager: NSObject {

    var host:String = ""
    var port:Int = 8080

    init(aHost:String, aPort:Int=8080) {
        super.init()
        
        host = aHost
        port = aPort
    }
    
    //MARK: Get
    
    func get(endPoint:String, parameters:Dictionary<String, String> = [:], completionHandler:@escaping(Error?, AnyObject?) -> ()) {
        guard let url:URL = composeUrl(aHost: host, aEndPoint: endPoint, aParams: parameters) else { return }
        
        loadingState(isActive: true)
        
        let dataTask:URLSessionDataTask = URLSession.shared.dataTask(with: url) { (data, response, err) in
            if let result = data {
                let json = self.parseData(data: result) as AnyObject
                completionHandler(nil, json)
            } else if let error = err {
                print(error.localizedDescription)
                completionHandler(error, nil)
            }
            self.loadingState(isActive: false)
        }
        
        DispatchQueue.global(qos: .background).async {
            dataTask.resume()
        }
    }
    
    private func parseData(data:Data) -> AnyObject? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue:0)) as AnyObject
            return json
        } catch let error as NSError{
            print("The error in the catch block is \(error)")
            return nil
        }
    }
    
    //MARK: Url
    
    private func composeUrl(aHost:String = "", aEndPoint:String = "", aParams:Dictionary<String, String> = [:], aPort:Int = 443) -> URL? {
        let urlString = "\(aHost):\(aPort)/\(aEndPoint)"
        var fullUrl:URL? = url(fromString: urlString)
        
        if let constUrl = fullUrl, aParams.keys.count > 0 {
            fullUrl = appendParams(url: constUrl, paremeters: aParams)
        }
        
        return fullUrl
    }
    
    private func url(fromString:String) -> URL? {
        return URL(string:fromString)
    }
    
    //MARK: Params
    
    private func appendParams(url:URL, paremeters:Dictionary<String, String>) -> URL {
        let urlComponents = NSURLComponents(string: url.absoluteString)!
        urlComponents.queryItems = dictionaryToQueryItems(dictionary: paremeters)
        return urlComponents.url!
    }
    
    private func dictionaryToQueryItems(dictionary:Dictionary<String, String>) -> [URLQueryItem] {
        var items:[URLQueryItem] = []
        
        for (key, value) in dictionary {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        return items;
    }
    
    //MARK: Loading UI
    
    private func loadingState(isActive:Bool) {
        DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = isActive
        }
    }
}
